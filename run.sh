#/bin/bash
echo -e "\
****\n\
** erocci is a occi front-end interface\n\
** developed in the framework of the OCCIware project\n\
****\n\
Now (re)starting the erocci container..."
docker restart demo_erocci
echo -n "Port mapping: "
docker inspect --format='{{range $p, $conf := .NetworkSettings.Ports}} {{$p}} -> {{(index $conf 0).HostPort}} {{end}}' demo_erocci
echo "To stop the container 'docker stop demo_erocci'"